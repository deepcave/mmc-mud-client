package RStream;
use strict;
use integer;

use CL; # force registering CL::Timer and CL::Socket

sub TIEHANDLE {
    my $class=shift;
    my $self={ printer => \&CL::msg , buffer => '' };
    $self->{printer}=\&CL::err if ($_[0]);
    bless $self,$class;
}

sub PRINT {
    my $self=shift;
    for my $s (@_) {
	my @sl=split(/\n/,$s,-1);
	$sl[0]=$self->{buffer} . $sl[0];
	$self->{buffer}=pop(@sl);
	for my $l (@sl) { $l =~ tr/\000-\037/ /s; &{$self->{printer}}($l) }
    }
}

sub DESTROY {
    my $self=shift;
    &{$self->{printer}}($self->{buffer}) if $self->{buffer};
}

my $last_msg='';
BEGIN { # stop perl from bitching to stdout
  if (!$::moddep_run) {
    $SIG{__WARN__}=sub {
      my $em=$_[0]||"";
      return if($last_msg eq $em);
      $last_msg=$em;
      return if $em =~ /^Unquoted string/; # ignore these warnings deliberately
      CL::emsg(0,'perl warn',$em);
    };
    $SIG{__DIE__}=sub {
      my $em=$_[0]||"";
      return if($last_msg eq $em);
      $last_msg=$em;
      CL::emsg(1,'perl die',$em);
      return;
    }
  }
}

1;