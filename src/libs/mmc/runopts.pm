package runopts;
#package to parse command line options especially -i to push it to @INC

use Conf;

BEGIN {
  sub getopt($$) {
    my $oh=$_[1];
    my %of=map { length($_)==1 ? ($_,1) : (substr($_,0,1),2) } split(/(?!:)/,$_[0]);
    while (@ARGV) {
      if ($ARGV[0] =~ /^--/) { shift(@ARGV); return undef; }
      if ($ARGV[0] =~ /^-$/) { shift(@ARGV); return "Invalid option: '-'"; }
      if ($ARGV[0] =~ /^-(.+)/) {
        my $os=$1;
        shift(@ARGV);
        while (length($os)>0) {
  	my $o=substr($os,0,1);
  	$os=substr($os,1);
  	if (!$of{$o}) { return "Invalid option: '$o'"; }
  	if ($of{$o}==1) {
  	  $oh->{$o}=1;
  	} else {
  	  if (length($os)>0) {
  	    push(@{$oh->{$o}},$os);
  	    $os='';
  	  } else {
  	    if (@ARGV) {
  	      push(@{$oh->{$o}},shift(@ARGV));
  	    } else {
  	      return "Option '$o' requires an argument";
  	    }
  	  }
  	}
        }
      } else {
        return undef;
      }
    }
  }
  
  sub parse_line {
      my($delimiter, $keep, $line) = @_;
      my($word, @pieces);

      #no warnings 'uninitialized';	# we will be testing undef strings

      while (length($line)) {
  	$line =~ s/^(["'])			# a $quote
          	    ((?:\\.|(?!\1)[^\\])*)	# and $quoted text
  		    \1				# followed by the same quote
  		   |				# --OR--
  		   ^((?:\\.|[^\\"'])*?)		# an $unquoted text
  		    (\Z(?!\n)|(?-x:$delimiter)|(?!^)(?=["']))  
  		    				# plus EOL, delimiter, or quote
  		  //xs or return;		# extended layout
  	my($quote, $quoted, $unquoted, $delim) = ($1, $2, $3, $4);
  	return() unless( defined($quote) || length($unquoted) || length($delim));

          if ($keep) {
  	    $quoted = "$quote$quoted$quote";
  	}
          else {
  	    $unquoted =~ s/\\(.)/$1/sg;
  	    if (defined $quote) {
  		$quoted =~ s/\\(.)/$1/sg if ($quote eq '"');
  		#$quoted =~ s/\\([\\'])/$1/g if ( $PERL_SINGLE_QUOTE && $quote eq "'");
              }
  	}
          $word .= substr($line, 0, 0);	# leave results tainted
          $word .= defined $quote ? $quoted : $unquoted;

          if (length($delim)) {
              push(@pieces, $word);
              push(@pieces, $delim) if ($keep eq 'delimiters');
              undef $word;
          }
          if (!length($line)) {
              push(@pieces, $word);
  	}
      }
      return(@pieces);
  }

  if ($^O eq "MSWin32") {
    #require Text::ParseWords;
    if (@ARGV && defined($ARGV[0])) {
      @ARGV=parse_line(qr/\s+/,0,$ARGV[0]);
    } else {
      @ARGV=();
    }
  }
  $::em=getopt("cf:h?i:knwprs:teb",\%::opt);
  if(!$::em) {
    $Conf::use_bin=1 if($::opt{b});
    # adjust include paths
    push(@INC,@{$::opt{i}}) if $::opt{i};
    # add the dir from which we were executed to the include path
    if(defined $::rundir) {
      my $t=0;
      for(my $x=0;$x<=$#INC;$x++) {
        if($INC[$x] eq $::rundir) {
          $t=1;
          last;
        }
      }
      push(@INC,$::rundir) if $::rundir && !$t;
    }
  }
}
1;